package br.com.mastertech.cliente.controllers;

import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping("/clientes")
    public Iterable<Cliente> listar(){
        return clienteService.listar();
    }

    @GetMapping("/cliente/{id}")
    public Cliente buscarPorId(@PathVariable(name = "id") @Valid int id){
        return clienteService.buscarPorId(id);
    }

    @PostMapping("/cliente")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Cliente> salvar(@RequestBody @Valid Cliente cliente){
        //try {
            return ResponseEntity.status(201).body(clienteService.salvar(cliente));
        /*}catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }*/
    }

}
