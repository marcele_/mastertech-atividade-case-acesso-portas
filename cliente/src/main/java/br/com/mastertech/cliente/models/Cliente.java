package br.com.mastertech.cliente.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotBlank(message = "O nome do cliente não pode estar em branco/vazio.")
    @NotNull(message = "O nome do cliente não pode ser igual a null.")
    @Size(min=2, max = 400, message = "O nome do cliente deve ter no mínimo 2 caracteres.")
    private String nome;

    public Cliente() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
