package br.com.mastertech.cliente.repositories;

import br.com.mastertech.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}