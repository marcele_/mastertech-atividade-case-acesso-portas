package br.com.mastertech.cliente.services;

import br.com.mastertech.cliente.models.Cliente;
import br.com.mastertech.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvar(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscarPorId(int id){
        return clienteRepository.findById(id).get();
    }

    public Iterable<Cliente> listar(){
        return clienteRepository.findAll();
    }
}
