package br.com.mastertech.porta.services;

import br.com.mastertech.porta.models.Porta;
import br.com.mastertech.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta salvar(Porta porta){
        return portaRepository.save(porta);
    }

    public Porta buscarPorId(int id){
        return portaRepository.findById(id).get();
    }

    public Iterable<Porta> listar(){
        return portaRepository.findAll();
    }
}
