package br.com.mastertech.porta.controllers;

import br.com.mastertech.porta.models.Porta;
import br.com.mastertech.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping("/porta")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Porta> salvar(@Valid @RequestBody Porta porta){
        //try{
            return ResponseEntity.status(201).body(portaService.salvar(porta));
        /*}catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }*/
    }

    @GetMapping("/portas")
    public Iterable<Porta> listar(){
        return portaService.listar();
    }

    @GetMapping("/porta/{id}")
    public Porta buscarPorId(@Valid @PathVariable(name = "id") int id){
        return portaService.buscarPorId(id);
    }

}
