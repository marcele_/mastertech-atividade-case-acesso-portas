package br.com.mastertech.porta.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotNull(message = "O andar não pode ser null")
    @NotBlank(message = "O andar não pode estar em branco.")
    @Size(min = 1, max = 150, message = "O andar deve ser preenchido")
    private String andar;

    @NotNull(message = "A sala não pode ser null")
    @NotBlank(message = "A sala não pode estar em branco.")
    @Size(min = 1, max = 400, message = "A sala deve ser preenchida")
    @Column
    private String sala;

    public Porta() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
