package br.com.mastertech.porta.repositories;

import br.com.mastertech.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
}
