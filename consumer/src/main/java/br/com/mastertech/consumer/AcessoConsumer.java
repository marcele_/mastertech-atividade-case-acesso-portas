package br.com.mastertech.consumer;

import br.com.mastertech.acesso.models.dtos.AcessoResposta;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-marcele-menezes", groupId = "marcele-1")
    public void receber(@Payload AcessoResposta acessoResposta) throws IOException, CsvDataTypeMismatchException,
            CsvRequiredFieldEmptyException {
        System.out.println("O acesso do cliente " + acessoResposta.getIdCliente() + " na porta " +
                acessoResposta.getIdPorta() + " = " + acessoResposta.isLiberado());

        List<AcessoResposta> acessos = new ArrayList<>();
        acessos.add(acessoResposta);

        //Writer writer = Files.newBufferedWriter(Paths.get("acessos.csv"), true);

        File file = new File("LogAcessos.csv");
        Writer writer = new FileWriter(file, true);

        StatefulBeanToCsv<AcessoResposta> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(acessos);

        writer.flush();
        writer.close();
    }

}
