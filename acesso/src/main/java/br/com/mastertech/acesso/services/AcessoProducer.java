package br.com.mastertech.acesso.services;

import br.com.mastertech.acesso.models.dtos.AcessoResposta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, AcessoResposta> producer;

    public void enviarAoKafka(AcessoResposta acessoResposta) {
        producer.send("spec3-marcele-menezes", acessoResposta);
    }

}
