package br.com.mastertech.acesso.clients.cliente;

import br.com.mastertech.acesso.models.Cliente;

import java.util.Optional;

public class ClienteClientFallback implements ClienteClient {

    /*@Override
    public Cliente getCarroByPlaca(String placa) {

//        Carro carro = new Carro();
//
//        carro.setModelo("Galinha pintadinha de rodas");
//        carro.setPlaca("PIU-PIU3");
//
//        return carro;
        throw new CarroOfflineException();
    }*/

    @Override
    public Optional<Cliente> buscarPorId(int id){
        throw new ClienteOfflineException();
    }

}
