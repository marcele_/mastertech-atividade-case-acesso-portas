package br.com.mastertech.acesso.clients.porta;

import br.com.mastertech.acesso.models.Porta;

import java.util.Optional;

public class PortaClientFallback implements PortaClient {
    /*@Override
    public Cliente getCarroByPlaca(String placa) {

//        Carro carro = new Carro();
//
//        carro.setModelo("Galinha pintadinha de rodas");
//        carro.setPlaca("PIU-PIU3");
//
//        return carro;
        throw new CarroOfflineException();
    }*/

    @Override
    public Optional<Porta> buscarPorId(int id){
        throw new PortaOfflineException();
    }

}
