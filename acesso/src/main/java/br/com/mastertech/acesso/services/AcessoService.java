package br.com.mastertech.acesso.services;

import br.com.mastertech.acesso.clients.cliente.ClienteClient;
import br.com.mastertech.acesso.clients.porta.PortaClient;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.models.Cliente;
import br.com.mastertech.acesso.models.Porta;
import br.com.mastertech.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    public Acesso salvar(Acesso acesso){
        Optional<Cliente> clienteOptional = clienteClient.buscarPorId(acesso.getIdCliente());
        if (!clienteOptional.isPresent()){
            throw new RuntimeException("O cliente "+ acesso.getIdCliente() +" não foi encontrado!");
        }

        Optional<Porta> portaOptional = portaClient.buscarPorId(acesso.getIdPorta());
        if (!portaOptional.isPresent()){
            throw new RuntimeException("A porta "+ acesso.getIdPorta() +" não foi encontrada!");
        }

        Optional<Acesso> acessoOptional = acessoRepository.findByIdPortaAndIdCliente(acesso.getIdPorta(), acesso.getIdCliente());
        if (acessoOptional.isPresent()) {
            throw new RuntimeException("O cliente " + acesso.getIdCliente() + " já tem acesso a porta " + acesso.getIdPorta());
        }

        return acessoRepository.save(acesso);
    }

    public Iterable<Acesso> listar(){
        return acessoRepository.findAll();
    }

    public Acesso buscarPorIds(Acesso acesso){
        Optional<Acesso> acessoOptional = acessoRepository.findByIdPortaAndIdCliente(acesso.getIdPorta(), acesso.getIdCliente());
        if (!acessoOptional.isPresent())
            throw new RuntimeException("Não há registro para liberação do cliente "+ acesso.getIdCliente() +" na porta "+ acesso.getIdPorta());

        return acessoOptional.get();
    }

    public void deletar(Acesso acesso){
        acesso = buscarPorIds(acesso);
        acessoRepository.delete(acesso);
    }
}
