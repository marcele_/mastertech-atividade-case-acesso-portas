package br.com.mastertech.acesso.clients.porta;

import br.com.mastertech.acesso.models.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.Optional;

//@FeignClient(name = "porta", url = "http://localhost:8081/porta/")
@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
//@FeignClient(name = "porta")
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Optional<Porta> buscarPorId(@Valid @PathVariable(name = "id") int id);

}
