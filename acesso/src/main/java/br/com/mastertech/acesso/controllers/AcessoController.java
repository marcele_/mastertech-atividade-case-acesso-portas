package br.com.mastertech.acesso.controllers;

import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.models.dtos.AcessoResposta;
import br.com.mastertech.acesso.services.AcessoProducer;
import br.com.mastertech.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Random;

@RestController
@RequestMapping
public class AcessoController {

    @Autowired
    private  AcessoService acessoService;

    @Autowired
    private AcessoProducer acessoProducer;

    /*@PostMapping
    public void create(@RequestBody Acesso acesso) {
        acessoProducer.enviarAoKafka(acesso);
    }*/

    @PostMapping("/acesso")
    public ResponseEntity<Acesso> salvar(@Valid @RequestBody Acesso acesso){
        //try {
            return ResponseEntity.status(201).body(acessoService.salvar(acesso));
        /*} catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }*/
    }

    @GetMapping("/acessos")
    public Iterable<Acesso> listar(){
        return acessoService.listar();
    }

    @GetMapping("/acesso/{cliente_id}/{porta_id}")
    public Acesso buscarPorIds(@Valid @PathVariable(name="cliente_id") int idCliente,
                                         @PathVariable(name = "porta_id") int idPorta){
        Acesso acesso = new Acesso();
        acesso.setIdCliente(idCliente);
        acesso.setIdPorta(idPorta);
        Acesso acessoEncontrado = acessoService.buscarPorIds(acesso);

        AcessoResposta acessoResposta = new AcessoResposta();
        acessoResposta.setIdCliente(acessoEncontrado.getIdCliente());
        acessoResposta.setIdPorta(acessoEncontrado.getIdPorta());

        final Random random = new Random();
        int indicador = random.nextInt(2);

        if ( indicador == 0 ) {
            acessoResposta.setLiberado(false);
        } else {
            acessoResposta.setLiberado(true);
        }
        System.out.println("Enviando mensagem... Cliente: " + acessoResposta.getIdCliente() +
                " | Porta: " + acessoResposta.getIdPorta() + " | Acesso: " + acessoResposta.isLiberado() +
                " | Indicador = " + indicador);
        acessoProducer.enviarAoKafka(acessoResposta);

        return acessoEncontrado;

    }

    @DeleteMapping("/acesso/{cliente_id}/{porta_id}")
    public ResponseEntity<?> deletar(@Valid @PathVariable(name = "cliente_id") int idCliente,
                                     @PathVariable(name = "porta_id") int idPorta) {
        try {
            Acesso acesso = new Acesso();
            acesso.setIdCliente(idCliente);
            acesso.setIdPorta(idPorta);

            acessoService.deletar(acesso);

            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }
}
