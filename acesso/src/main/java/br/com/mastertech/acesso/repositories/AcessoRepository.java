package br.com.mastertech.acesso.repositories;

import br.com.mastertech.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {

    Optional<Acesso> findByIdPortaAndIdCliente(int idPorta, int idCliente);



}
