package br.com.mastertech.acesso.clients.cliente;

import br.com.mastertech.acesso.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.Optional;

//@FeignClient(name = "cliente", url = "http://localhost:8081/cliente/")
@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
//@FeignClient(name = "cliente")
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Optional<Cliente> buscarPorId(@PathVariable(name = "id") @Valid int id);

}
